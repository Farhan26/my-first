from django.shortcuts import render

def Home(request):
	return render(request,'fix1.html')

def Profile(request):
	return render(request,'fix2.html')

def Experiences(request):
	return render(request,'fix3.html')

# Create your views here.
